Software Name: gnuplot_ci
Purpose: Provides an interface through which GNUPlot commands could be
called from a C/C++ program
Author: Swagat Kumar 
Email: swagat.kumar@gmail.com
License: GNU Public License (GPL) Version 2.0 or higher
---------------------------------------

Pre-requisites:
- GNU/Linux Operating System: Ubuntu 14.04 or Mint 17 
- G++ (4.8.2) 
- cmake (2.8.12.2)
- gnuplot 4.6 patchlevel 4

-----------------------------------------

Compile / Install Instruction

$ cd gnuplot_ci/
$ mkdir build
$ cd build
$ cmake ../
$ make
$ make install  

It creates a library "libgnuplot_ci.a" inside the folder 'lib' in the
main project directory. You can also copy it manually.

----------------------------------------------------------------------

Logs: November 22, 2014 
- Added a new function "draw3DCoordAxes" to the class. 

Logs: January 31, 2016
- Updated draw_cuboid2() function. It can now draw multiple boxes in the
same plot.

- draw_cuboid_solid(): A new function added that can plot colored box.
                       It still has some bug. It can not plot multiple
                       boxes with different colors. 


