/* C++ Interface to GNUPLOT
 * gnuplot test file
 * -------------------------*/

#include<iostream>
#include<cmath>
#include <gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;   // Define the namespace

#define CUBOID
//#define COORD3DAXES

int main()
{
  // Creating a handle for GNUPLOT
  // Pass the path of gnuplot binary as first string
  GP_handle G1("/usr/bin/", "X", "Y", "Z");   

#ifdef CUBOID
  // Any gnuplot command can be passed like this
  G1.gnuplot_cmd("set terminal wxt");
  G1.gnuplot_cmd("set border");

  // Draw a cuboid whose left-bottom and top-right points are given
  //G1.draw_cuboid(-0.1,0.3,0,0.1,0.5,0.4);

    double x1[3] = {-0.1,-0.1,-0.1};
    double x2[3] = {0.1,-0.2,0};
    double x3[3] = {-0.1, 0.3, 0.0};

    double dx[3] = {0.2, 0.2, 0.2};
    double dx2[3] = {0.2, 0.2, 0.4};



    G1.draw_cuboid2(x3, dx2);
    G1.draw_cuboid_solid(x1,dx,1);
    getchar();
    G1.draw_cuboid_solid(x2,dx,2);



  //G1.draw_sphere(0.0,0.4,0.2,0.25,10);
  //G1.gnuplot_cmd("replot 'gp_cuboid.txt' w l");
  
#endif

#ifdef COORD3DAXES

  G1.gnuplot_cmd("splot '../src/test.txt' u 1:2:3 w p \n");
  G1.gnuplot_cmd("set style arrow 4 head filled size 0.05,20,0 lt 4 lw 2"); 

 double dx[3] = {0.2,0.2,0.2};
 double xp[3] = {0.0, 0.0, 0.0};
 double xp2[3] = {1.0, 1.0, 1.0};
 double xn[3] = {1.1, 1.1, 0.86};
 double yn[3] = {0.97, 1.17, 1.1};
 double zn[3] = {1.17, 0.97, 1.1};

 G1.draw3dcoordAxis(xp, dx);
 G1.draw3dcoordAxis(xp2, xn, yn, zn);

 int arrow_cnt = G1.get_arrow_cnt();

 char cmd[100];
 sprintf(cmd, "set arrow %d from 0,0,0 to 1,1,1 as 4", arrow_cnt+1);
 G1.gnuplot_cmd(cmd);
 G1.gnuplot_cmd("replot"); 
 G1.gnuplot_cmd("show arrow");
 
#endif
  
  //Otherwise gnuplot window won't stay on screen
  // Use pause command instead
 cout << "Press Enter to Exit the program ...." << endl;
  getchar(); 
  return 0;
}
  
