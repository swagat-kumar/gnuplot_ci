#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


/* Read characters from the pipe and echo them to stdout. */
void read_from_pipe(int file)
{
  FILE *stream;
  int c;
  stream = fdopen (file, "r");
  while ((c = fgetc (stream)) != EOF)
    putchar (c);
  fclose (stream);
}
/* Write some random text to the pipe. */

void 
write_to_pipe (int file)
{
  FILE *stream;
  stream = fdopen (file, "w");
  fprintf (stream, "hello, world!\n");
  fprintf (stream, "goodbye, world!\n");
  fclose (stream);
}
int
main (void)
{
  pid_t pid ;
  int mypipe[2];

  /* Create the pipe. Returns 0 on success */
  if (pipe (mypipe))
    {
      fprintf (stderr, "Pipe failed.\n");
      return EXIT_FAILURE;
    }

  /* Create the child process. */
  pid = fork ();
  if (pid == (pid_t) 0)
    {
      /* This is the child process. */
      dup2(mypipe[1], 1); // mypipe[1] is now standard output
      dup2(mypipe[0], 0); // mypipe[0] is now standard input
      execl("/usr/bin/gnuplot" "gnuplot",  (char*)NULL);
      return EXIT_SUCCESS;
    }
  else if (pid < (pid_t) 0)
    {
      /* The fork failed. */
      fprintf (stderr, "Fork failed.\n");
      return EXIT_FAILURE;
    }
  else
    {
      /* This is the parent process. */

      char read_buff[1000];
      char write_buff[1000];

      FILE *INPUT = fdopen(mypipe[0], "r");
      FILE *OUTPUT = fdopen(mypipe[1], "w");

      fprintf(OUTPUT, "plot sin(x)\n");
      fprintf(OUTPUT, "print GPVAL_PLOT\n");

      int plot_var;
      while(fscanf(INPUT, "%d", &plot_var) !=EOF)
      {
        printf("plot_var = %d\n", plot_var);
      }


      fclose(INPUT);
      fclose(OUTPUT);

      getchar();
      close(mypipe[0]);
      close(mypipe[1]);
      return EXIT_SUCCESS;
    }
}


